import 'package:flutter/material.dart';
import 'package:sampleResponsive/constants.dart';
import 'package:sampleResponsive/utils.dart';
// import 'package:try_recycling_app/widgets/app_bar.dart';

// import '../../utils/constants.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  List<Widget> cards = List<Widget>();

  // Platform messages are asynchronous, so we initialize in an async method.

  buildCard(String label, String screen) {
    return GestureDetector(
      onTap: () {
        print('Presed: $label');
        // (label == 'Videos')
        //     ? launchURL(VIDEOS_URL)
        //     : Navigator.pushNamed(context, screen);
        Navigator.pushNamed(context, screen);
      },
      child: Card(
        elevation: 5,
        color: MAIN_COLOR,
        child: Padding(
          padding: EdgeInsets.all(7),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 5, top: 5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 1.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    label,
                                    style: TextStyle(
                                      fontSize: 17.0,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Align(
                                alignment: Alignment.bottomRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 10.0),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/' +
                                            screen.replaceAll('/', '') +
                                            '.jpg',
                                        fit: BoxFit.contain,
                                        scale: 9.0,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    cards.add(
      buildCard(
        'Mercury',
        // 'calendar',
        MERCURY_UI,
      ),
    );
    cards.add(
      buildCard(
        'Venus',
        // 'alerts',
        VENUS_UI,
      ),
    );
    cards.add(
      buildCard(
        'Earth',
        // 'location',
        EARTH_UI,
      ),
    );
    cards.add(
      buildCard(
        'Mars',
        // 'qa',
        MARS_UI,
      ),
    );
    cards.add(
      buildCard(
        'Jupyter',
        // 'tips',
        JUPYTER_UI,
      ),
    );
    cards.add(
      buildCard(
        'Saturn',
        // 'videos',
        SATURN_UI,
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: SafeArea(
            child: getAppBar(
              context,
              'Dashboard',
            ),
          ),
        ),
        body:
            // OrientationBuilder(
            // builder: (context, orientation) {
            // print('__ orientation: $orientation');
            GridView.count(
          shrinkWrap: true,
          childAspectRatio: 1.2,
          crossAxisCount: 2,
          // crossAxisCount: orientation == Orientation.portrait
          //     ? 2
          //     : 3, //crossAxisCount: 2,
          children: [
            for (int i = 0; i < cards.length; i++) cards[i],
          ],
        )
        // },
        // ),
        );
  }
}
